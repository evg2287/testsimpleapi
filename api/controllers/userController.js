import jwt from 'jsonwebtoken';

import utils from '../../utils/index';
import config from '../../config/index';
import userService from '../../services/userService';
import emailService from '../../services/emailService';

class UserController {
  async createUser(req, res, next) {
    try {
      const {
        name, password, email, last_name,
      } = req.body;

      if (!name || !password || !email || !last_name) {
        res.status(400).json('Invalid data!');
        return;
      }

      const result = userService.createUser(req.body);
      res.json(result);
    } catch (err) {
      console.error(err);
      next(err);
    }
  }

  async forgotPassword(req, res, next) {
    try {
      const email = req.query.email.toLowerCase();
      if (!email || !utils.isEmail(email)) {
        res.status(400).json('Invalid email');
        return;
      }

      const result = await emailService.sendMail(email);
      res.json(result);
    } catch (err) {
      console.error(err);
      next(err);
    }
  }

  async resetPassword(req, res, next) {
    try {
      const { token } = req.query;
      const { password } = req.body;
      const { email } = jwt.verify(token, config.jwtSecret);
      if (!password || password.length < 6) {
        res.status(400).json('Invalid new password');
        return;
      }

      if (!email || !utils.isEmail(email)) {
        res.status(400).json('Something wrong');
        return;
      }

      const result = userService.resetPassword(password, email, token);
      res.json(result);
    } catch (err) {
      console.error(err);
      next(err);
    }
  }

  async getUser(req, res, next) {
    try {
      const result = userService.getUser();
      res.json(result);
    } catch (err) {
      console.error(err);
      next(err);
    }
  }
}

export default new UserController();
