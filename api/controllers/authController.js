import authService from '../../services/authService';

class AuthController {
  auth(req, res, next) {
    try {
      const { email, password } = req.body;
      // eslint-disable-next-line no-useless-escape
      const regExp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (!email || !regExp.test(email)) {
        res.status(400).json('Invalid email');
        return;
      }

      if (!password || password.length < 6) {
        res.status(400).json('Invalid password');
        return;
      }

      const result = authService.auth(email.toLowerCase(), password);
      if (!result) {
        res.status(400).json('User not found');
        return;
      }

      res.json({ accessToken: result });
    } catch (err) {
      console.error(err);
      next(err);
    }
  }
}

export default new AuthController();
