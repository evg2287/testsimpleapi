import express from 'express';

import { errorRoute, errorHandler } from './middlewares/errorHandler';
import userRouter from './routes/userRouter';
import authRouter from './routes/authRouter';

const api = express.Router();

api.use('/v1/user', userRouter);
api.use('/v1/auth', authRouter);
api.use(errorRoute, errorHandler);

export default api;
