import express from 'express';

import auth from '../middlewares/authCheck';
import userController from '../controllers/userController';

const userRouter = express.Router();

userRouter.get('/', auth, (req, res, next) => userController.getUser(req, res, next));
userRouter.post('/signUp', (req, res, next) => userController.createUser(req, res, next));
userRouter.get('/forgot', (req, res, next) => userController.forgotPassword(req, res, next));
userRouter.patch('/reset', (req, res, next) => userController.resetPassword(req, res, next));

export default userRouter;
