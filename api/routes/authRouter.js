import express from 'express';

import authController from '../controllers/authController';

const userRouter = express.Router();

userRouter.post('/signIn', (req, res, next) => authController.auth(req, res, next));

export default userRouter;
