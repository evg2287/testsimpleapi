/* eslint-disable max-len */
/**
 * @swagger
 * components:
 *   schemes: [http, https]
 *   securitySchemes:
 *     bearerAuth:
 *       type: http
 *       scheme: bearer
 *       bearerFormat: JWT
 *   schemas:
 *     Auth:
 *       type: object
 *       properties:
 *         email:
 *           type: string
 *           description: Email
 *         password:
 *           type: string
 *           description: Password
 *       example:
 *         email: test@gmail.com
 *         password: qwerty
 *     User:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the user
 *         name:
 *           type: string
 *           description: User name
 *         last_name:
 *           type: string
 *           description: User LastName
 *         age:
 *          type: int
 *          description: User age
 *         email:
 *          type: string
 *          description: User email
 *         password:
 *          type: string
 *          description: User password
 *         role:
 *          type: string
 *          description: User role
 *         token:
 *          type: string
 *          description: The auto-generated token for user
 *       example:
 *         id: 4,
 *         name: 'John'
 *         last_name: 'Doe'
 *         age: 666
 *         email: 'enii@improveit.solutions'
 *         password: 'qwerty'
 */

/**
 * @swagger
 * /api/v1/user:
 *   get:
 *     summary: Returns the list of all users (protected route, need token)
 *     tags: [User]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: The list of the users
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 *       401:
 *          description: Invalid token
 */

/**
 * @swagger
 * /api/v1/user/signUp:
 *   post:
 *     summary: User signUp
 *     tags: [User]
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/User'
 *     responses:
 *       200:
 *         description: Get all user data
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       400:
 *         description: Bad request
 */

/**
 * @swagger
 * /api/v1/user/forgot:
 *   get:
 *     summary: Send reset password link to email
 *     tags: [User]
 *     parameters:
 *       - in: query
 *         name: email
 *         schema:
 *           type: string
 *         required: true
 *     responses:
 *       200:
 *         description: Message check email
 *       400:
 *          description: Invalid Email
 *       500:
 *         description: Some server error
 */

/**
 * @swagger
 * /api/v1/user/reset:
 *  patch:
 *    summary: Reset password
 *    tags: [User]
 *    requestBody:
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            type: object
 *            properties:
 *              password:
 *                type: string
 *                example: Anaconda54321
 *    parameters:
 *      - in: query
 *        name: token
 *        schema:
 *          type: string
 *        required: true
 *        description: Reset token
 *    responses:
 *      200:
 *        description: The password was changed
 *      400:
 *        description: Invalid password
 */

/**
 * @swagger
 * /api/v1/auth/signIn:
 *   post:
 *     summary: SingIn
 *     tags: [Auth]
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Auth'
 *     responses:
 *       200:
 *         description: Return token
 *       400:
 *         description: Invalid data
 */
