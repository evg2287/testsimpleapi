Create a simple API Authentication Node app (registration, login, reset password, and a protected route - let's say dashboard).
There is no need for any type of storage, make use of hardcoded collections (objects) that for example will allow the user: "ol@example.com" to authenticate with the password "qwerty". As for the registration, what matters is the response upon validation.

Suggestions: SWAGGER (that allows testing and object examples)

NOTE: there is no need for any kind of ui. Please pay attention to the edge cases and the common authentication / registration problems and validations.