import jwt from 'jsonwebtoken';
import crypto from 'crypto';

import config from '../config/index';
import db from '../database/usersDB';

class UserService {
  createUser(data) {
    const {
      name, last_name, age, email, password,
    } = data;

    const id = crypto.createHash('sha256').update(JSON.stringify(data)).digest('base64');
    const token = jwt.sign({ id, role: 'user' }, config.jwtSecret, { expiresIn: config.jwtTokenLife });
    const userData = {
      id, name, last_name, age, email: email.toLowerCase(), password, role: 'user', token,
    };
    const user = db.find((el) => email.toLowerCase() === el.email);
    if (user) return 'User already exists';
    db.push(userData);
    return userData;
  }

  resetPassword(newPass, email, token) {
    const user = db.find((el) => email === el.email);
    if (user.token !== token) {
      return 'Bad token';
    }
    user.password = newPass;
    return 'Password was changed';
  }

  getUser() {
    return db;
  }
}

export default new UserService();
