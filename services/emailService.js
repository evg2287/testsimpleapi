import nodemailer from 'nodemailer';
import jwt from 'jsonwebtoken';

import db from '../database/usersDB';
import config from '../config/index';

class EmailService {
  constructor() {
    this.transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: config.gmailEmail,
        pass: config.gmailPassword,
      },
      tls: { rejectUnauthorized: false },
    });
  }

  async sendMail(email) {
    // eslint-disable-next-line max-len
    const token = jwt.sign({ email, reset: true }, config.jwtSecret, { expiresIn: config.jwtTokenLife });
    const user = db.find((el) => email === el.email);
    if (user) {
      user.token = token;
      await this.transporter.sendMail({
        from: config.gmailEmail,
        to: email,
        subject: 'TEST Reset pass TEST',
        text: `${config.host}/api/v1/user/reset?token=${token}`,
      });
    }
    return 'Please check your email!';
  }
}

export default new EmailService();
