/* eslint-disable consistent-return */
/* eslint-disable no-param-reassign */
/* eslint-disable array-callback-return */
/* eslint-disable max-len */
import jwt from 'jsonwebtoken';

import config from '../config/index';
import db from '../database/usersDB';

class AuthService {
  auth(email, password) {
    const user = db.find((el) => el.email === email && el.password === password);
    if (!user) return null;
    const token = jwt.sign({ id: user.id, role: user.role }, config.jwtSecret, { expiresIn: config.jwtTokenLife });
    user.token = token;
    return user.token;
  }
}

export default new AuthService();
