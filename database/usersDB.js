const users = [
  {
    id: 1,
    name: 'John',
    last_name: 'Doe',
    age: 666,
    email: 'test@gmail.com',
    password: 'qwerty',
    role: 'admin',
    token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IndpZlRKU09WWVkyNzUxRnJvMlBXa0hIRi9BbUUvWm1IOVNBQTBvWk01Mnc9Iiwicm9sZSI6InVzZXIiLCJpYXQiOjE2MzMwODk5NTksImV4cCI6MTYzMzA5NzE1OX0.CM2bFm4ogxfqDc1WeUQKiXSy8qNLGXCk8F3LHxJ98K0',
  },
  {
    id: 'wifTJSOVYY2751Fro2PWkHHF/AmE/ZmH9SAA0oZM52w=',
    name: 'John',
    last_name: 'Doe',
    age: 666,
    email: 'test2@gmail.com',
    password: 'qwerty',
    role: 'user',
    token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IndpZlRKU09WWVkyNzUxRnJvMlBXa0hIRi9BbUUvWm1IOVNBQTBvWk01Mnc9Iiwicm9sZSI6InVzZXIiLCJpYXQiOjE2MzMwODk5NTksImV4cCI6MTYzMzA5NzE1OX0.CM2bFm4ogxfqDc1WeUQKiXSy8qNLGXCk8F3LHxJ98K0',
  },
];

export default users;
