// eslint-disable-next-line no-useless-escape
const EMAIL_PATTERN = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

function isEmail(mail) {
  return EMAIL_PATTERN.test(mail);
}

export default { isEmail };
