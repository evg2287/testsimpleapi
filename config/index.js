import dotenv from 'dotenv';

dotenv.config();

const swaggerOptions = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Simple API',
      version: '1.0.0',
      description: 'A simple Express API',
    },
    servers: [
      {
        url: 'https://mighty-hamlet-00185.herokuapp.com',
      },
      {
        url: 'http://localhost:3059',
      },
    ],
  },
  apis: ['./api/views/*.js'],
};

export default {
  host: process.env.HOST || 'https://mighty-hamlet-00185.herokuapp.com',
  port: process.env.PORT || 3059,
  jwtSecret: process.env.SECRET || '6BjRkLTwO6rg4KTp6638V0j2kKxnBpZI',
  jwtTokenLife: process.env.SECRET_LIFE || '2h',
  gmailEmail: process.env.GMAIL_MAIL,
  gmailPassword: process.env.GMAIL_PASSWORD,
  swaggerOptions,
};
