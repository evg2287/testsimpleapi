import express from 'express';
import cors from 'cors';
import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUI from 'swagger-ui-express';

import config from './config/index';
import api from './api/index';

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

const specs = swaggerJsdoc(config.swaggerOptions);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(specs));

app.use('/api', api);

app.listen(config.port, () => console.info(`Service has started on port ${config.port}`));
